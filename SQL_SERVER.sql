-- INICIO
  exec sp_tables; --mostrar tablas
  exec sp_columns nombre_tabla; --ver la estructura de la tabla

  GO --Es un comando que nos permite ejecutar multiples procedimientos en un SCRIPT, es la señal de finalización de un lote de sentencias


-- TIPOS DE DATOS EN SQL ////////////////////////////////////
  --CARACTERES
    x -- cantidad de caracteres
    varchar(x) --cadenas de texto variables
    char(x) --cadenas de texto fiajs
    text -- valores de más de 8000 caracteres

    --NUMEROS ENTEROS
    integer -- -2000000000 a 2000000000
    smallint -- -32000 a 32000
    tinyint -- 0 a 255
    bigint -- -9000000000000000000 a 9000000000000000000

    --FLOTANTES
    decimal(x,y) --numeros flotantes exactos o sea X numeros enteros y x numeros decimales
    float --8 bytes
    real --4 bytes

    --DINERO
    money -- hasta 19 digitos y 4 decimales
    smallmoney entre -- -200000.3648 y 200000.3647

    --FECHAS
    --Los valores tipo fecha se ingresan entre comillas simples y se ermiten separadores como / - .
    datetime --desde 01/01/1753 al 31/12/9999
    smalldatetime --desde el 01/1900 al 06/06/2079
    /*
    --FORMATOS FECHA 
      -mdy  month day year  POR DEFECTO SQL UTILIZA ESTE FORMATO
      -myd month year day
      -dmy day month year
      -dym day year month
      -ydm year day month
      -ymd year month day
    */
      set dateformat mdy -- se utiliza para ingresar una fecha con formato
      -- TODOS LAS FECHAS DATATIME SE MUESTRAN COMO YEAR MONTH DAY HOURS MINS SEGS MILISEGS
    


-- CREAR TABLA /////////////////////////////////////////////
  create usuarios(
    nombre varchar(30),
    clave varchar(10),
    edad integer
  );

  -- NULL valor desconocido o inexistente, si un campo no deberia estar vacío se especifica con 'not null'
  create usuarios(
    nombre varchar(30) not null,
    clave varchar(10),
    edad integer
  );

  -- PRIMARY KEY
  create table usuarios(
    id varchar(10) not null,
    nombre varchar(30) not null,
    clave varchar(30),
    edad integer,
    primary key(id)
  );

  -- atributo IDENTITY (valor con 1 y se auto incrementa automaticamente)
  create table usuarios(
    id integer identity,
    nombre varchar(30) not null,
    clave varchar(30),
    edad integer,
    primary key(id)
  );

  -- tambien se puede indicar el valor de inicio de la secuencia y el incremento
  create table usuarios(
    id integer identity(100, 5), -- inicia en 100 e incrementa de 5 en 5
    nombre varchar(30) not null,
    clave varchar(30),
    edad integer,
    primary key(id)
  );

  -- DEFAULT PERMITE ASIGNARLE UN VALOR POR DEFECTO A UN CAMPO
  create table usuarios(
    id integer identity(100, 5), -- inicia en 100 e incrementa de 5 en 5
    nombre varchar(30) not null default 'desconocido',
    clave varchar(30),
    edad integer,
    primary key(id)
  );


-- BORRAR TABLA /////////////////////////////////////////////
  if object_id('usuarios') is not null
    drop table usuarios;


-- INSERTAR REGISTROS A LA TABLA ////////////////////////////
  insert into usuarios(nombre, clave, edad) values('Alberto', 'abc1234', 17);

  insert into usuarios(nombre, clave, edad) values('Juan', 'def1234', 17);

  insert into usuarios(nombre, clave, edad) values('Pedro', null, 34);

  insert into usuarios(nombre, clave, edad) values('Miguel', 'klmn1234', 21);

  insert into usuarios values('alberto', 'xd123', 19);

  insert into usuarios(nombre, edad) values('Miguel', 21);


-- RECUPERAR REGISTROS DE UNA TABLA /////////////////////////
  select * from usuarios; -- * indica seleccionar todos los campos

  select nombre, edad from usuarios;

  -- WHERE : nos permite especificar condiciones para una consulta select
  select nombre, clave from usuarios
    where edad = 17;

  -- para recuperar registros con valor NULL se utilizan los operadores 'is null' y 'is not null'
  select * from usuarios
    where clave is not null;

  select * from usuarios
    where clave is null;

  --BETWEEN trabaja con intervalo de valores
  select * from libros
    where between 20 and 40; --devuelve lo que esta entre 20 y 40

  select * from libros
    where not between 20 and 40; --devuelve lo que esta fuera del intervalo entre 20 y 40

  -- IN se utiliza para averiguar si los calores de una lista se encuentran en el valor de un campo
  select * from libros
    where autor in ('alberto', 'miguel'); --recuperamos los libros cuyo autor sea alberto o miguel
  
   select * from libros
    where autor not in ('alberto', 'miguel'); --recuperamos los libros cuyo autor NO sea alberto o miguel
  

-- OPERADORES ///////////////////////////////////////////////
  /*
  -- RELACIONALES 
    = igual
    <> distinto
    < menor
    > mayor
    >= mayor o igual
    <= menor o igual
  */
    select nombre, clave from usuarios
      where edad < 30;
  /*
  --ARITMÉTICOS
    + suma
    - resta
    * multiplicación
    / división
    % módulo
  */
    select titulo, precio, cantidad, precio*cantidad from libros;

    select titulo, precio, precio-(precio * 0.1) from libros;

  /*
  --DE CONCATENACIÓN
    + operador de concatenación
  */
    select titulo+' '+precio from libros;
  /*
  --LÓGICOS
    AND todas las condiciones deben ser verdaderas
    OR una de las condiciones sea verdadera
    NOT invierte el resultado de la condición
  */
    select titulo, precio, cantidad from libros
      where (precio > 500) and (precio < 1000);

-- ALIAS 'as'
  select nombre+' '+edad as 'nombre y edad' from usuarios;


-- ELIMINAR REGISTROS //////////////////////////////////////
  delete from usuarios
    where nombre = 'Miguel'; -- sin el WHERE se borra todos los registros de la tabla

  --TRUNCATE : elimina todos los registros (es más rapido y reinicia los identity)
  truncate table usuarios;


-- UPDATE ///////////////////////////////////////////////////
--permite modificar uno ovarios datos en un registro
  update usuarios set clave = 'default'; --sin el where se actualizan todos los registros de la tabla

  update usuarios set clave = 'clavesegura1234'
    where nombre = 'Juan';


-- FUNCIONES ///////////////////////////////////////////////
  -- CADENAS
    -- substring('cadena',inicio,longitud); Devuelve una subcadena de una 'cadena' pasada como parametro desde el 'inicio' y con una 'longitud' pasada como parametro.
    select substring('Buenas tardes', 8, 6); --devuelve 'tardes' 

    -- str(numero,longitud,cantidad de decimales); convierte un 'numero' en un caracter. 'longitud' es la cantidad de enteros maximos que recibe la función.
    select str(123.456, 7, 3); --devuelve '123.456'
    select str(123.456); --devuelve '123' (Por defecto tiene longitud 10 y cantidad de decimales 0)
    select str(123.456, 2, 3); --devuelve '**' numero mayor al parametro longitud

    --stuf(cadena1,inicio,longitud,cadena2)
    --Inserta una cadena en otra. Elimina una 'longitud' determinada de caracteres de la 'cadena1' a partir de la posición de 'inicio' y, a continuación, inserta la 'cadena2' en la 'cadena1', en la posición de 'inicio'.
    select stuff('123456789', 3, 4, 'hola'); --devuelve '12hola789'

    --len(cadena); devuelve la longitud de la 'cadena'.
    select len('hola'); --devuelve 4
    
    --char(valor); devuelve el caractes de código ASCII correspondiente al 'valor'.
    select char(65); --Devuelve 'A'

    --left(cadena,longitud); --devuelve una subcadena desde el primer caracter hasta la 'longitud'.
    select left('123456789', 7) --devuelve '1234567'

    --right(cadena,longitud); -devuelve una subcadena desde el ultimo caracter caracter hasta la 'longitud'.
    select right('123456789', 7); -- devuelve '3456789'.

    --lower(cadena); --devuelve la cadena en minusculas
    select lower('HOla'); --devuelve 'hola'

    --upper(cadena); --devuelve la cadena en mayusculas
    select lower('HOla'); --devuelve 'HOLA'

    --ltrim(cadena); elimina espacios de la izquierda
    select ltrim('      hola mundo'); --devuelve 'hola mundo'
    
    --rtrim(cadena); elimina espacios de la derecha
    select rtrim('hola mundo        '); --devuelve 'hola mundo'

    --replace(cadena,subcadena A, subcadena B); --reemplaza en la 'cadena' todas las ocurrencias de la 'subcadena A' por lo que tiene la 'subcadena B'
    select replace('hola mundo soy del mundo tierra, el mejor mundo', 'mundo', 'planeta'); --devuelve 'hola planeta soy del planeta tierra, el mejor planeta'.

    --reverse(cadena); --devuelve la cadena invertida
    select reverse('12345'): -- devuelve '54321'

    --patindex(patron,cadena) -- devuelve la posicion de comienzo de la primera ocurrencia del 'patron' especificado en la 'cadena' si no encuentra el 'patron retorna 0.
    select patindex('%luis%', 'jorge luis borges'); -- retorna 7
    select patindex('%or%', 'jorge luis borges'); -- retorna 2
    select patindex('%ar%', 'jorge luis borges'); -- retorna 0

    --charindex(subcadena,cadena,inicio) --Devuelve la posicion donde comienza 'subcadena' dentro de 'cadena' iniciando la busqueda desde 'inicio'. si no se especifica 'inicio' toma el valor de 0. si no encuentra la 'subcadena' devuelve 0.
    select charindex('or', 'jorge luis borges',5); --devuelve 13
    select charindex('or', 'jorge luis borges'); --devuelve 2
    select charindex('or', 'jorge luis borges',14); --devuelve 0

    --replicate(cadena,cantidad) --repite la 'cadena' 'cantidad' de veces 
    select replicate('hola',4); --retorna 'holaholaholahola'

    --space(cantidad) --retorna una cadena de espacion de longitud 'cantidad'
    select 'hola'+space(5)+'mundo'; --devuelve 'hola     mundo'.

  --SE PUEDEN EMPLEAR ESTA FUNCIONES ENVIANDO COMO ARGUMENTO EL NOMBRE DE UN CAMPO DE TIPO CARACTER. EJEMPLO:

    create table empleados(
      nombre varchar(30),
      edad integer
    );

    insert into empleados values(alberto,22);
    insert into empleados values(miguel,23);
    insert into empleados values(juan,25);

    select left(nombre,2) as abreviacion, edad from empleados;
    -- retorna:
      -- |abreviacion | edad|
      -- |    al      | 22  |
      -- |    mi      | 23  |
      -- |    ju      | 25  |
  
  -- MATEMATICAS
    -- abs(x) retorna el valor absoluto de 'x'
    select abs(-10); --retorna 10.

    -- ceiling(x); retorna el valor de 'x' redondeado hacia arriba.
    select ceiling(9.7); --retorna 10.

    -- floor(x); retorna el valor de 'x' redondeado hacia abajo.
    select floor(9.7); --retorna 9.

    -- power(x, y); retorna el valor de 'x' elevado a la 'y' potencia.
    select power(2,3); --retorna 8.

    -- round(numero, longitud);retorna el 'numero' redondeado a la 'longitud' especificada. si 'longitud es negativo el numero es redondeado desde la parte entera segun el valor de longitud.
    select round(123.456,1); -- retorna 123.500
    select round(123.456,2); -- retorna 123.460
    select round(123.456,-1); -- retorna 120.456

    -- sign(x); devuelve -1 si 'x' es negativo, 1 si es positivo y 0 si es 0.
    select sign(230); --retorna 1.

    --square(x); devuelve el valor de 'x' elevado al cuadrado.
    select square(2); --devuelve 4
    -- srqt(x); devuelve la raiz cuadrada de 'x'.
    select srqt(4); --retorna 2.

  --SE PUEDEN EMPLEAR ESTA FUNCIONES ENVIANDO COMO ARGUMENTO EL NOMBRE DE UN CAMPO DE TIPO NUMERICO. ejemplo:

    select nombre, square(edad) as 'edad al cuadrado' from empleados;
    -- retorna:
      -- |nombre |edad al cuadrado|
      -- |alberto|     484        |
      -- |miguel |     529        |
      -- |juan   |     625        |

  -- FECHAS Y HORAS
    --getdate(); --retorna la fecha y hora actual.
    
    --datepart(parte, fecha); --retorna l parte especifica de una fecha. Los valores de 'parte' pueden ser: year, quarter,month, day, week, hour, minute, second y milisecond.
    select datepart(hour, getdate()); -- retorna la hora actual
    select datepart(month, getdate()); --retorna mes actual

    --dateadd(parte, numero, fecha); -- retorna 'fecha' sumandole 'numero' a la 'parte' seleccionada, day, month, year, etc.
    set dateformat ymd;
    select dateadd(day,3,'1980/11/02'); -- retorna 1980-11-05 00:00:00:000
    select dateadd(month,3,'1980/11/02'); -- retorna 1980-02-02 00:00:00:000

    --datediff(parte, fecha1, fecha2); intervalo de tiempo entre 'fecha1' y 'fecha2', retorna un valor correspondiente a 'fecha2'-'fecha1'.
    select datediff(day, '2005/10/28', '2006/10/28') --retorna 365
    select datediff(month, '2005/10/28', '2006/10/28') --retorna 12

    --day(fecha); --retorna el dia de 'fecha'
    --month(fecha); --retorna el mes de 'fecha'
    --year(fecha); --retorna el año de 'fecha'

  --SE PUEDEN EMPLEAR ESTA FUNCIONES ENVIANDO COMO ARGUMENTO EL NOMBRE DE UN CAMPO DE TIPO datatime o smalldatatime.

-- ORDENAR REGISTROS ///////////////////////////////////////
  --Para ordenar resultados de un select se utiliza la clausula 'ORDER BY'.
  select * from nombre_tabla
    order by campo;
  
  --igual se puede colocar un numero que referencie el campo deacuerdo su posicion en la lista de seleccion
  select titulo, autor, precio from libros
    order by 3; --esto hará que se ordere deacuerdo al precio.

  select * from libros
    order by editorial desc; --por defecto se ordena ascendentemente con la palabra clave 'desc' se puede ordenar descententemente
  
  select * from libros
    order by titulo, editorial --se puede ordenar por varios campos
  
  select * from libros
    order by titulo asc, editoral desc; -- incluso en distintos sentidos
  
  -- Es posible ordenar por un campo que no se lista en la selección
  select titulo, autor from libros
    order by precio;

  -- Se permite ordenar por valores calculados o expresiones
  select titulo, autor, precio-(precio*0.1) as rebaja from libros
    order by 4;

  -- Order by, no puede emplearse para campos text, ntext e image.

-- BUSQUEDA DE PATRONES
  -- LIKE Y NOT LIKE son operadores relacionales que se usan para realizar comparaciones exclusivamente de cadena que permiten comparar trozos de cadena de caracteres para realizar consultas. el simbolo % reemplaza cualquier cantidad de caracteres, funge como un comodin.
  select * from libros 
    where autor like '%borges%'; --devolveria los valores que contenga borges ejemplo: J.L borges. y almiborgestran

  select * from libros
    where titulo like 'M%'; -- devuelve libros que comienzen con M
  
  select * from libros
    where titulo not like 'M%'; -- devuelve libros que NO comienzen con M

  select * from libros
    where autor like '%carrol_'; -- el simbolo _ igualmente es un comodín pero reemplaza unicamente un caractec. Por lo tanto retornaria carroll y carrolt pero no carroline

  --otro caracter comodín es [] Halla coincidencias con cada uno de los caracteres del intervalo o conjunto especificado entre corchetes
  select titulo, autor, editorial from libros
    where editoral like '[P-S]%'; --retorna libros cuya editorial comience con P, Q, R Y S.

  select titulo, autor, editorial from libros
    where editoral like 'A[nm]%'; --retorna libros cuya editorial comience con 'An' o 'Am'.
  
  -- ^ coincide con cualquier carácter único que NO se encuentre dentro del intervalo o del conjunto especificado entre corchetes [^]   
  select titulo, autor, editoral from libros
    where editorial like 'Al[^a]%'; -- retorna libros cuya editorial comienzen con 'Al' y tengan una tercera letra que NO sea 'a'
  
  --para busquedas de caracteres comodines como literales, debemos incluirlo dentr de conchetes
  select titulo, autor, editoral from libros
    where titulo like '%[_]]%'; -- retorna libros cuyo titulo contenga '_'
  
-- CONTAR REGISTROS
  --count() -- cuenta lo cantidad de registros de una tabla, incluyendo los que tienen valor nulo
  select count(*) from libros; -- retorna la cantidad de registros incluyendo los que contengan NULL.

  select count(*) from libros
    where editorial = 'porrua' -- retorna la cantidad de registros cuya editorial sea 'porrua' incluyendo los que contengan NULL.

  select count(precio) from libros -- retorna la cantidad de registros que tienen precio (No toma en cuenta los NULL).

  --count_big() --similar a count() pero count_big() retorna un valor bigint y count() un int.
  select count_big(distinct editorial) from libros; -- devuelve la cantidad de registros DISTINTOS en el campo editorial.
  
-- FUNCIONES DE AGRUPAMIENTO









-- 